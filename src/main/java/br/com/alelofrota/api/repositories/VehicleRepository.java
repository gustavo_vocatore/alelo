package br.com.alelofrota.api.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.alelofrota.api.models.Vehicle;

@Repository
public interface VehicleRepository  extends CrudRepository<Vehicle ,Long>{
	
	List<Vehicle> findAll();
	
	List<Vehicle> findByPlateContaining(String plate);
	
	List<Vehicle> findByStatus(Boolean status);
	
	Boolean existsByPlate(String plate);
	
}