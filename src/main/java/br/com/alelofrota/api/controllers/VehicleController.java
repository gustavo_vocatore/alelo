package br.com.alelofrota.api.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.alelofrota.api.models.Vehicle;
import br.com.alelofrota.api.models.response.MessageResponse;
import br.com.alelofrota.api.services.VehicleService;

@RestController
@RequestMapping("/vehicle")
@CrossOrigin(origins = "*", allowCredentials= "", allowedHeaders="*")
public class VehicleController {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(VehicleController.class);
	
	@Autowired
	private VehicleService vehicleService;
	
	@GetMapping(value = "", produces = "application/json; charset=UTF-8")
	public Page<Vehicle> paginatedSearch(
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer pagina,
			@RequestParam(name = "limit", required = false, defaultValue = "10") Integer limite) {
		return vehicleService.paginateFindByCriteria(pagina, limite);
	}
	
	@GetMapping(value = "", produces = "application/json; charset=UTF-8", params="filter")
	public List<Vehicle> findByPlate(
			@RequestParam(name = "filter", required = false, defaultValue = "") String filter) {
		return vehicleService.filter(filter);
	}
	
	@GetMapping(value = "/{id}", produces = "application/json; charset=UTF-8")
	public Vehicle findById(@PathVariable("id") Long id) {
		return vehicleService.findById(id);
	}
	
	@PostMapping(value = "", consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
	public ResponseEntity<MessageResponse> save(@RequestBody Vehicle vehicle){
		vehicleService.save(vehicle);
		return new ResponseEntity<MessageResponse>(new MessageResponse("Cadastrado com sucesso"), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
	public ResponseEntity<MessageResponse> update(@PathVariable("id") Long id, @RequestBody Vehicle vehicle){
		vehicleService.update(vehicle, id);
		return new ResponseEntity<MessageResponse>(new MessageResponse("Alterado com sucesso"), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = "application/json; charset=UTF-8")
	public ResponseEntity<MessageResponse> delete(@PathVariable("id") Long id) {
		vehicleService.delete(id);
		return new ResponseEntity<MessageResponse>(new MessageResponse("Deletado com sucesso"), HttpStatus.OK);
	}
}
