package br.com.alelofrota.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.alelofrota.api.exceptions.BadRequestException;
import br.com.alelofrota.api.models.Vehicle;
import br.com.alelofrota.api.repositories.VehicleRepository;

@Service
public class VehicleService {
	
	@Autowired
	private VehicleRepository vehicleRepository;

	public Page<Vehicle> paginateFindByCriteria(Integer pagina, Integer limite) {
		Pageable pageable = PageRequest.of(pagina, limite);
		int start = (int) pageable.getOffset();
		List<Vehicle> vehicles = vehicleRepository.findAll();
		int end = (int) ((start + pageable.getPageSize()) > vehicles.size() ? vehicles.size()
				  : (start + pageable.getPageSize()));
		return new PageImpl<Vehicle>(vehicles.subList(start, end),pageable, vehicles.size());
	}
	
	public List<Vehicle> filter(String filter) {
		if(filter.equals("true") || filter.equals("false"))
			return vehicleRepository.findByStatus(Boolean.getBoolean(filter));
		else
			return vehicleRepository.findByPlateContaining(filter);
	}
	
	public Vehicle findById(Long id) {
		return vehicleRepository.findById(id).orElse(new Vehicle());
	}
	
	public void save(Vehicle vehicle) {
		Boolean validaPlate = vehicleRepository.existsByPlate(vehicle.getPlate());
		if(validaPlate)
			throw new BadRequestException("There is already a vehicle with this plate");
		vehicleRepository.save(vehicle);
	}
	
	public void update(Vehicle vehicle, Long id) {
		Vehicle valdaVehicle = vehicleRepository.findById(id).orElse(null);
		if(valdaVehicle == null)
			throw new BadRequestException("Vehicle not found");
		vehicleRepository.save(vehicle);
	}
	
	public void delete(Long id) {
		Vehicle valdaVehicle = vehicleRepository.findById(id).orElse(null);
		if(valdaVehicle == null)
			throw new BadRequestException("Vehicle not found");
		vehicleRepository.deleteById(id);
	}
}
