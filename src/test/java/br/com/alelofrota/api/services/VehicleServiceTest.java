package br.com.alelofrota.api.services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.alelofrota.api.models.Vehicle;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class VehicleServiceTest {
	
	@Autowired
	private VehicleService vehicleService;

	@Test
	public void shouldByPage() {
		Page<Vehicle> page = vehicleService.paginateFindByCriteria(0, 5);
		System.out.println(page);
	}
	
	@Test
	public void filter() {
		List<Vehicle> vehicles = vehicleService.filter("AAAAAA");
		System.out.println(vehicles);
	}
	
	@Test
	public void findById() {
		Vehicle vehicle = vehicleService.findById(1L);
		System.out.println(vehicle);
	}
	
	@Test
	public void save() {
		Vehicle vehicle = new Vehicle();
		vehicle.setColor("black");
		vehicle.setManufacturer("teste");
		vehicle.setModel("aaa");
		vehicle.setPlate("AAAAAAA");
		vehicle.setStatus(true);
		vehicleService.save(vehicle);
	}
	
	@Test
	public void update() {
		Vehicle vehicle = new Vehicle();
		vehicle.setColor("black");
		vehicle.setManufacturer("teste");
		vehicle.setModel("aaa");
		vehicle.setId(1L);
		vehicle.setPlate("AAAAAAA");
		vehicle.setStatus(true);
		vehicleService.update(vehicle,1L);
	}
	
	@Test
	public void delete() {
		vehicleService.delete(1L);
	}
}