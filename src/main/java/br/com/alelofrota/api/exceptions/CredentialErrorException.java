package br.com.alelofrota.api.exceptions;

public class CredentialErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CredentialErrorException(String message) {
	    super(message);
	}
}