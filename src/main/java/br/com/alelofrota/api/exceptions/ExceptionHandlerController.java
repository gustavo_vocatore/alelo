package br.com.alelofrota.api.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.source.InvalidConfigurationPropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.alelofrota.api.models.response.MessageResponse;

/**
 * Controller que monitora outros controllers da aplicação, captura as exceções
 * lançadas por eles e trata-as.
 */

@ControllerAdvice
public class ExceptionHandlerController {
	
	@Autowired
	private ObjectMapper mapper;

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> Exception(Exception exception) {
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvalidConfigurationPropertyValueException.class)
	public ResponseEntity<?> resourceNotFoundException(InvalidConfigurationPropertyValueException exception) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(CredentialErrorException.class)
	public ResponseEntity<String> credentialEmptyException(CredentialErrorException exception) throws JsonProcessingException {
		MessageResponse message = new MessageResponse(exception.getMessage());
		return new ResponseEntity<String>(mapper.writeValueAsString(message), HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<String> credentialEmptyException(BadRequestException exception) throws JsonProcessingException {
		MessageResponse message = new MessageResponse(exception.getMessage());
		return new ResponseEntity<String>(mapper.writeValueAsString(message), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(BindException.class)
	public ResponseEntity<String> bindException(BindException exception) throws JsonProcessingException {
		MessageResponse message = new MessageResponse(exception.getFieldError().getDefaultMessage());
		return new ResponseEntity<String>(mapper.writeValueAsString(message), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> methodArgumentNotValidExceptionException(MethodArgumentNotValidException exception) throws JsonProcessingException {
		MessageResponse message = new MessageResponse(exception.getBindingResult().getFieldError().getDefaultMessage());
		return new ResponseEntity<String>(mapper.writeValueAsString(message), HttpStatus.BAD_REQUEST);
	}
}